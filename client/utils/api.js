import { httpRequest } from './http.js'
// 话题排行
export function httpTopicTop() {
	return httpRequest({
		url: 'topic/top',
		method: 'GET'
	})
}
// 搜索排行
export function httpSearchTop() {
	return httpRequest({
		url: 'search/top',
		method: 'GET'
	})
}
// 笔记列表
export function httpNoteList(params) {
	return httpRequest({
		url: 'note/list',
		method: 'GET',
		data: params
	})
}
// 笔记详情
export function httpNoteDetail(id, userId) {
	return httpRequest({
		url: 'note/detail',
		method: 'GET',
		data: {
			id,
			userId
		}
	})
}
// 删除笔记
export function httpNoteDelete(noteId, userId) {
	return httpRequest({
		url: 'note',
		method: 'DELETE',
		data: {
			noteId,
			userId
		}
	})
}
// 搜索
export function httpNoteSearch(params) {
	return httpRequest({
		url: 'note/search',
		method: 'GET',
		data: params
	})
}
// 发布笔记
export function httpNoteRelease(data) {
	return httpRequest({
		url: 'note/create',
		method: 'POST',
		data
	})
}
// 用户信息
export function httpUserInfo(id) {
	return httpRequest({
		url: 'user/info?userId=' + id,
		method: 'GET'
	})
}
// 用户的关注
export function httpUserInfoFocus(id) {
	return httpRequest({
		url: 'user/focus?userId=' + id,
		method: 'GET'
	})
}
// 用户的粉丝
export function httpUserInfoFans(id) {
	return httpRequest({
		url: 'user/fans?userId=' + id,
		method: 'GET'
	})
}
// 编辑用户信息
export function httpUserEditInfo(userId, data) {
	return httpRequest({
		url: 'user/info',
		method: 'PUT',
		data: {
			id: userId,
			...data
		}
	})
}
// 修改密码
export function httpUserEditPassword(userId, password) {
	return httpRequest({
		url: 'user/password',
		method: 'PUT',
		data: {
			id: userId,
			password
		}
	})
}
// 修改手机号
export function httpUserEditPhone(userId, phone) {
	return httpRequest({
		url: 'user/phone',
		method: 'PUT',
		data: {
			id: userId,
			phone
		}
	})
}
// 用户密码登录
export function httpUserLogin(phone, password) {
	return httpRequest({
		url: 'user/login',
		method: 'POST',
		data: {
			phone,
			password
		}
	})
}
// 用户注册
export function httpUserRegister(phone, password, name) {
	return httpRequest({
		url: 'user/register',
		method: 'POST',
		data: {
			phone,
			password,
			name
		}
	})
}
// 获取用户笔记
export function httpUserNote(userId, params) {
	return httpRequest({
		url: 'user/note?userId=' + userId,
		method: 'GET',
		data: params
	})
}
// 获取用户收藏
export function httpUserFavorite(userId, params) {
	return httpRequest({
		url: 'user/favorite?userId=' + userId,
		method: 'GET',
		data: params
	})
}
// 获取用户点赞
export function httpUserLike(userId, params) {
	return httpRequest({
		url: 'user/like?userId=' + userId,
		method: 'GET',
		data: params
	})
}
// 用户点赞
export function httpUserToLike(userId, noteId, cancel = false) {
	return httpRequest({
		url: 'note/like',
		method: 'PUT',
		data: {
			userId,
			noteId,
			cancel: cancel
		}
	})
}
// 用户收藏
export function httpUserToFavorite(userId, noteId, cancel = false) {
	return httpRequest({
		url: 'note/favorite',
		method: 'PUT',
		data: {
			userId,
			noteId,
			cancel: cancel
		}
	})
}
// 关注
export function httpUserFocus(userId, fansId, cancel) {
	return httpRequest({
		url: 'user/focus',
		method: 'POST',
		data: {
			userId,
			fansId,
			cancel
		}
	})
}
// 评论
export function httpsComment(userId, noteId, content) {
	return httpRequest({
		url: 'msg/comment',
		method: 'POST',
		data: {
			userId,
			noteId,
			content
		}
	})
}
// 回复
export function httpsReply(userId, replyUserId, noteId, content, commentId) {
	return httpRequest({
		url: 'msg/reply',
		method: 'POST',
		data: {
			userId,
			replyUserId,
			noteId,
			content,
			commentId
		}
	})
}
// 加载笔记下的评论
export function httpsLoadComment(noteId) {
	return httpRequest({
		url: 'msg/comment?noteId=' + noteId,
		method: 'GET'
	})
}
// 获取关注关系
export function httpUserRelationship(userId, currentUserId) {
	return httpRequest({
		url: 'user/relationship',
		method: 'GET',
		data: {
			userId,
			currentUserId
		}
	})
}
// 删除评论或回复
export function httpMsgDelete(params) {
	return httpRequest({
		url: `msg`,
		method: 'DELETE',
		data: params
	})
}
// 获取消息
export function httpUserNews(userId) {
	return httpRequest({
		url: 'user/news?userId=' + userId,
		method: 'GET'
	})
}
// 获取系统消息
export function httpUserNewsBySystem(userId) {
	return httpRequest({
		url: 'user/news/system?userId=' + userId,
		method: 'GET'
	})
} 
// 已读
export function httpNewsRead(newsId) {
	return httpRequest({
		url: 'user/news/read',
		method: 'PUT',
		data: {
			newsId
		}
	})
}
// 删除消息
export function httpNewsDelete(newsId) {
	return httpRequest({
		url: 'user/news',
		method: 'DELETE',
		data: {
			newsId
		}
	})
}