class Response {
    success(ctx, message = 'OK', data = undefined) {
        // ctx.status = 200
        ctx.body = {
            code: 200,
            message,
            data
        }
    }

    fail(ctx, message = 'Forbidden 禁止访问') {
        // ctx.status = 403
        ctx.body = {
            code: 403,
            message
        }
    }

    authFail(ctx, message = 'Unauthorized 需要token') {
        // ctx.status = 401
        ctx.body = {
            code: 401,
            message
        }
    }

    null(ctx, message = 'Not Found 查无此资源') {
        // ctx.status = 404
        ctx.body = {
            code: 404,
            message
        }
    }

    exist(ctx, message = '已存在', data = undefined) {
        // ctx.status = 412
        ctx.body = {
            code: 412,
            message,
            data
        }
    }

    error(ctx, err = undefined) {
        // ctx.status = 500
        ctx.body = {
            code: 500,
            message: 'Internal Server Error 服务器错误，请联系管理员',
            err
        }
    }

}

module.exports = new Response