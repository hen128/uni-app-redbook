class InitManager {
    static initCore(app) {
        // 入口方法
        InitManager.app = app
        InitManager.loadResponse()
        InitManager.loadConfig()
        InitManager.loadMonent()
        InitManager.loadUtils()
    }

    // 全局 配置
    static loadConfig() {
        const config = require('../../config')
        global.config = config
    }

    // 全局 工具包
    static loadUtils() {
        global.utils = require('../utils')
    }

    // 时间格式化包
    static loadMonent() {
        const Moment = require('moment')
        global.Moment = Moment
    }

    // 返回
    static loadResponse() {
        const Response = require('./Response')
        global.Response = Response
    }
}

module.exports = InitManager