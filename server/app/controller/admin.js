const DB = require('../../config/DB')
const {
    Op
} = require('sequelize')
const { uMd5, uPutToken } = require('../utils/index')
const note = require('../models/note')
class AdminController {
    async loginAdminer(ctx) {
        const {
            account,
            password
        } = ctx.request.body
        const adminer = await DB.admin.findOne({
            where: {
                account,
                password: uMd5(password)
            }
        })
        if (adminer) {
            let lastLoginAt = await adminer.get('updatedAt')
            // 更新登录时间
            adminer.updatedAt = global.Moment().format('YYYY-MM-DD HH:mm:ss')
            await adminer.save()
            const data = {
                adminer: {
                    adminerId: adminer.id,
                    account: adminer.account,
                    name: adminer.name,
                    lastLoginAt
                },
                token: uPutToken(adminer.id)
            }
            global.Response.success(ctx, '登录成功', data)
        } else {
            global.Response.fail(ctx, '账号或密码错误')
        }
    }
    // 笔记列表
    async listNote(ctx) {
        // 参数
        let {
            page = 1, size = 10
        } = ctx.query
        page = Number(page)
        size = Number(size)
        const {
            count,
            rows
        } = await DB.note.findAndCountAll({
            offset: (page - 1) * size,
            limit: size,
            order: [
                ['createdAt', 'DESC']
            ],
            include: [{
                model: DB.user,
                attributes: ['id', 'name', 'avatar']
            }]
        })
        const data = {
            rows,
            meta: {
                size, // 每页条目
                count: count, // 条目总数
                page: page, // 当前页数
                total: Math.ceil(count / size), // 总页数
            }
        }
        global.Response.success(ctx, undefined, data)
    }
    // 审核笔记
    async checkNote(ctx) {
        const {
            noteId, check, content
        } = ctx.request.body
        const note = await DB.note.findByPk(noteId)
        note.check = check
        await note.save()
        if (check === 2) {
            // 新建消息
            await DB.news.create({
                title: '审核通知',
                content:'笔记不通过：' + content,
                to: note.get('userId'),
                read: 0,
                type: 3,
                noteId,
                createdAt: global.Moment().format('YYYY-MM-DD HH:mm:ss'),
                updatedAt: global.Moment().format('YYYY-MM-DD HH:mm:ss')
            })
        }
        global.Response.success(ctx, '更新成功')
    }
    // 用户列表
    async listUser(ctx) {
        // 参数
        let {
            page = 1, size = 10
        } = ctx.query
        page = Number(page)
        size = Number(size)
        const {
            count,
            rows
        } = await DB.user.findAndCountAll({
            offset: (page - 1) * size,
            limit: size,
            order: [
                ['id', 'DESC']
            ],
            attributes: { exclude: ['password'] }
        })
        const data = {
            rows,
            meta: {
                size, // 每页条目
                count: count, // 条目总数
                page: page, // 当前页数
                total: Math.ceil(count / size), // 总页数
            }
        }
        global.Response.success(ctx, undefined, data)
    }
    // 用户权限调整
    async checkUser(ctx) {
        const {
            userId, status
        } = ctx.request.body
        await DB.user.update({
            status
        }, {
            where: {
                id: userId,
            }
        })
        global.Response.success(ctx, '更新成功')
    }
    // 管理员列表
    async listAdminer(ctx) {
        // 参数
        let {
            page = 1, size = 10
        } = ctx.query
        page = Number(page)
        size = Number(size)
        const {
            count,
            rows
        } = await DB.admin.findAndCountAll({
            offset: (page - 1) * size,
            limit: size,
            order: [
                ['id', 'DESC']
            ]
        })
        const data = {
            rows,
            meta: {
                size, // 每页条目
                count: count, // 条目总数
                page: page, // 当前页数
                total: Math.ceil(count / size), // 总页数
            }
        }
        global.Response.success(ctx, undefined, data)
    }
}

module.exports = new AdminController
