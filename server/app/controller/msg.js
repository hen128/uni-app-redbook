const DB = require('../../config/DB')
const {
    Op
} = require('sequelize')
// DB.note.hasOne(DB.user)
// DB.user.hasMany(DB.note)
DB.note.belongsTo(DB.user)
DB.comment.belongsTo(DB.user)
class MsgController {
    async createComment(ctx) {
        let {
            userId,
            content,
            noteId,
            status = 1
        } = ctx.request.body
        const comment = await DB.comment.create({
            userId,
            content,
            noteId,
            status,
            createdAt: global.Moment().format('YYYY-MM-DD HH:mm:ss'),
            updatedAt: global.Moment().format('YYYY-MM-DD HH:mm:ss')
        })
        const note = await DB.note.findByPk(noteId)
        // 新建消息
        await DB.news.create({
            title: '新的评论',
            content,
            from: userId,
            to: note.get('userId'),
            read: 0,
            type: 1,
            commentId: comment.get('id'),
            noteId,
            createdAt: global.Moment().format('YYYY-MM-DD HH:mm:ss'),
            updatedAt: global.Moment().format('YYYY-MM-DD HH:mm:ss')
        })
        global.Response.success(ctx, undefined, comment)
    }
    // 加载笔记的评论
    async loadComments(ctx) {
        let {
            noteId
        } = ctx.query
        const comments = await DB.comment.findAll({
            where: {
                noteId,
                status: 1
            },
            include: [{
                model: DB.user,
                attributes: ['id', 'avatar', 'name']
            }],
            order: [
                ['createdAt', 'DESC']
            ]
        })
        // 加载回复
        for (let commentItem of comments) {
            const replies = await DB.reply.findAll({
                where: {
                    commentId: commentItem.get('id'),
                    status: 1 // 审核通过
                },
            })
            for (let row of replies) {
                let userId = row.get('userId')
                let replyUserId = row.get('replyUserId')
                // 回复用户
                let user = await DB.user.findByPk(userId, {
                    attributes: ['id', 'avatar', 'name']
                })
                // 被回复用户
                let replyUser = await DB.user.findByPk(replyUserId, {
                    attributes: ['id', 'avatar', 'name']
                })
                row.dataValues.user = user
                row.dataValues.replyUser = replyUser
            }
            commentItem.dataValues.replies = replies
        }
        global.Response.success(ctx, undefined, comments)
    }
    // 回复
    async createReply(ctx) {
        let {
            userId,
            replyUserId,
            content,
            commentId,
            status = 1
        } = ctx.request.body
        const reply = await DB.reply.create({
            userId,
            replyUserId,
            content,
            commentId,
            status,
            read: 0, // 未读
            createdAt: global.Moment().format('YYYY-MM-DD HH:mm:ss'),
            updatedAt: global.Moment().format('YYYY-MM-DD HH:mm:ss')
        })
        const comment = await DB.comment.findByPk(commentId)
        // 新建消息
        await DB.news.create({
            title: '新的回复',
            content,
            from: userId,
            to: replyUserId,
            read: 0,
            type: 2,
            commentId: comment.get('id'),
            replyId: reply.get('id'),
            noteId: comment.get('noteId'),
            createdAt: global.Moment().format('YYYY-MM-DD HH:mm:ss'),
            updatedAt: global.Moment().format('YYYY-MM-DD HH:mm:ss')
        })
        global.Response.success(ctx, undefined, comment)
        global.Response.success(ctx, undefined, reply)
    }
    // 删除评论或回复
    async deleteCommentOrReply(ctx) {
        const {
            commentId, replyId, userId
        } = ctx.request.body
        if (commentId) {
            await DB.comment.destroy({ // return 1 or 0
                where: {
                    id: commentId,
                    userId
                }
            })
            // 删除评论下的所有回复
            await DB.reply.destroy({ // return 1 or 0
                where: {
                    commentId
                }
            })
            global.Response.success(ctx, '删除成功')
        } else if (replyId) {
            await DB.reply.destroy({ // return 1 or 0
                where: {
                    id: replyId,
                    userId
                }
            })
            global.Response.success(ctx, '删除成功')
        }
    }
    async listComment(ctx) {
    }
    async check(ctx) {
    }
}

module.exports = new MsgController
