// const BasicAuth = require('basic-auth') // 提取token
const Jwt = require('jsonwebtoken') // 解析 token
const crypto = require('crypto') // 加密
const {
    jwt_config
} = require('../../config')
const secretKey = jwt_config.serect
const expiresIn = jwt_config.expiresIn
const fs = require('fs')

const utils = {
    // 颁发token
    uPutToken(uid, auth = 1000) { // scope 权限
        const user = {
            uid,
            auth
        }

        const token = Jwt.sign(user,
            secretKey, {
            expiresIn
        })

        return token
    },

    // 加密
    uMd5(str) {
        str += global.config.jwt_config.serect // 加密盐
        const md5 = crypto.createHash('md5')
        const result = md5.update(str).digest('hex') //hex表示拿到最终为十六进制
        return result
    },

    // 获取客户端IP
    getClientIP(req) {
        let ip = req.headers['x-forwarded-for'] || // 判断是否有反向代理 IP
            req.ip ||
            req.connection.remoteAddress || // 判断 connection 的远程 IP
            req.socket.remoteAddress || // 判断后端的 socket 的 IP
            req.connection.socket.remoteAddress || ''
        if (ip) {
            ip = ip.replace('::ffff:', '')
        }
        return ip
    },

    // 删除文件
    async uDeleteFile(url) {
        if (fs.existsSync(url)) {
            await fs.unlinkSync(url)
            return true
        } else {
            throw `给定的路径${url}不存在，请给出正确的路径`
        }
    },

    // 根据路径生成文件目录
    async uMakeDir(url) {
        if (!fs.existsSync(url)) {
            await fs.mkdirSync(url)
        }
        return true
    },

    // 读取文件
    async uWriteFile(url, content) {
        await fs.writeFileSync('.' + url, content)
        return true
    }
}

module.exports = {
    ...utils
}