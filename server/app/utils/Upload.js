const multer = require('koa-multer');
//配置    
var storage = multer.diskStorage({
    //文件保存路径
    destination(req, file, cb) {
        try {
            fileDir = `static/upload/${global.Moment().format('YYYY-MM-DD')}` //根目录 注意路径必须存在
            global.utils.uMakeDir(fileDir) // 生成日期目录
            cb(null, fileDir)
        } catch (err) {
            console.error(err)
        }
    },
    //修改文件名称
    filename(req, file, cb) {
        const fileFormat = (file.originalname).split(".");
        cb(null, Date.now() + "." + fileFormat[fileFormat.length - 1]);
    }
})


//加载配置
const Upload = multer({
    storage: storage
})

module.exports = Upload