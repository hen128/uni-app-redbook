const DB = require('../../config/DB')
const {
    Op
} = require('sequelize')
const fs = require('fs')
const { join } = require('path')

const baseUrl = 'http://127.0.0.1:3000'

class FileController {
    async listTree(ctx) {
        let tree = []

        let dirs = fs.readdirSync('static/upload')

        dirs.forEach((item, index) => {
            let obj = {
                label: item,
                children: []
            }
            let fPath = join('static/upload', item)
            let files = fs.readdirSync(fPath)
            files.forEach(file => {
                obj.children.push({ label: file })
            })

            tree.push(obj)
        })
        global.Response.success(ctx, undefined, tree)
    }


    // 获取文件详情
    async getFile(ctx) {
        const {
            id, name
        } = ctx.query

        let data = ''
        if (id) {
            data = await DB.file.findByPk(id)
        }
        if (name) {
            data = await DB.file.findOne({
                where: {
                    name
                }
            })
        }

        global.Response.success(ctx, undefined, data)
    }


    // 更新文件
    async updateFile(ctx) {
        let {
            id,
            content,
            title,
            url
        } = ctx.request.body

        try {
            let file

            if (id) {
                file = await DB.file.findByPk(id)
            } else if (url) {
                file = await DB.file.findOne({
                    where: {
                        url
                    }
                })
            }

            if (file) {
                // 不更新文件原名
                if (!title) title = file.get('title')

                // 更新文件内容
                if (content) {
                    let fileUrl = file.get('url')
                    fs.writeFileSync(`./static${fileUrl}`, content)
                }

                file.set({
                    updateAt: Moment().format('YYYY-MM-DD HH:mm:ss'),
                    title
                })

                await file.save()
            }


            global.Response.success(ctx, undefined, file)
        } catch (err) {
            console.error(err)
        }
    }

    // 删除文件
    async delFile(ctx) {
        const {
            id,
            url
        } = ctx.query

        if (id) {
            var file = await DB.file.findByPk(id)
            var fileUrl = 'static' + file.get('url')
            var res = await DB.file.destroy({
                where: {
                    id
                }
            })
        } else if (url) {
            var fileUrl = 'static' + url
            var res = await DB.file.destroy({
                where: {
                    url
                }
            })
        }

        if (res) {
            global.utils.uDeleteFile(fileUrl)
            global.Response.success(ctx, '删除成功')

        } else {
            global.Response.null(ctx)
        }
    }

    // 生成文件
    async createFile(ctx) {
        let {
            title,
            content,
            type
        } = ctx.request.body

        try {
            let fileName = Date.now()

            let dirName = global.Moment().format('YYYY-MM-DD')

            let fileDir = `static/upload/${dirName}` //根目录 注意路径必须存在
            global.utils.uMakeDir(fileDir) // 生成日期目录

            fs.writeFileSync(`./static/upload/${dirName}/${fileName}.${type}`, content)

            const params = {
                name: fileName + '.' + type,
                title: title + '.' + type,
                url: '/upload/' + dirName + '/' + fileName + '.' + type,
                type,
                size: '000', // 暂时未解决
                createAt: global.Moment().format('YYYY-MM-DD HH:mm:ss'),
                updateAt: global.Moment().format('YYYY-MM-DD HH:mm:ss')
            }

            const file = await DB.file.create(params)


            global.Response.success(ctx, undefined, file)
        } catch (err) {
            console.error(err)
        }
    }

    // 上传文件
    async uploadFile(ctx) {
        // const proof = ctx.req.body.proof || ''
        // const user = await DB.user.findOne({
        //     where: {
        //         proof
        //     }
        // })

        // 如果检测不到凭证，则拒绝上传
        // if (!user) {
        //     global.utils.uDeleteFile(ctx.req.file.destination + `/${ctx.req.file.filename}`)
        //     global.Response.fail(ctx, '上传失败，凭证错误')
        //     return false
        // }

        const params = {
            name: ctx.req.file.filename,
            title: ctx.req.file.originalname, // 原文件名 中文会乱码
            url: (ctx.req.file.destination + `/${ctx.req.file.filename}`).replace('static', ''),
            type: ctx.req.file.filename.substring(ctx.req.file.filename.lastIndexOf('.') + 1), // 文件类型
            size: ctx.req.file.size / (1024 * 1024),
            // userId: user.get('id'),
            createAt: Moment().format('YYYY-MM-DD HH:mm:ss'),
            updateAt: Moment().format('YYYY-MM-DD HH:mm:ss')
        }
        const file = await DB.file.create(params)
        // 添加http前缀
        file.url = baseUrl + file.url


        global.Response.success(ctx, undefined, file)
    }

    // 搜索文件
    async searchFile(ctx) {
        // 参数
        let {
            page = 1, size = 10, key = '' // 关键词
        } = ctx.query

        page = Number(page)
        size = Number(size)

        const {
            count,
            rows
        } = await DB.file.findAndCountAll({
            offset: (page - 1) * size,
            limit: size,
            order: [
                ['id', 'DESC']
            ],
            where: {
                title: {
                    [Op.like]: `%${key}%`
                }
            }
        })

        const data = {
            rows,
            meta: {
                key,
                size, // 每页条目
                count: count, // 条目总数
                page: page, // 当前页数
                total: Math.ceil(count / size), // 总页数
            }
        }
        global.Response.success(ctx, undefined, data)
    }

    // 获取文件列表
    async listFile(ctx) {
        // 参数
        let {
            page = 1, size = 10
        } = ctx.query

        page = Number(page)
        size = Number(size)

        const {
            count,
            rows
        } = await DB.file.findAndCountAll({
            offset: (page - 1) * size,
            limit: size,
            order: [
                ['id', 'DESC']
            ],
        })

        // for (let row of rows) {
        //     const userId = row.get('userId')
        //     if (userId) {
        //         const user = await DB.user.findByPk(userId)
        //         row.dataValues.userName = user.name
        //     }
        // }

        const data = {
            rows,
            meta: {
                size, // 每页条目
                count: count, // 条目总数
                page: page, // 当前页数
                total: Math.ceil(count / size), // 总页数
            }
        }
        global.Response.success(ctx, undefined, data)
    }
}

module.exports = new FileController
