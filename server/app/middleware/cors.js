const cors = require('koa2-cors') //跨域处理


const corsOptions = {
    origin: function (ctx) {
        // console.log(ctx.url)
        // if (ctx.url === '/test') {
        //     return false;
        // }
        return '*';
    },
    // origin: function (ctx) { //设置允许来自指定域名请求
    //     const whiteList = ['http://zengweihao.cn', 'http://admin.zengweihao.cn', 'http://server.zengweihao.cn']; //可跨域白名单
    //     let url = ctx.header.referer.substr(0, ctx.header.referer.length - 1);
    //     if (whiteList.includes(url)) {
    //         return url //注意，这里域名末尾不能带/，否则不成功，所以在之前我把/通过substr干掉了
    //     }
    //     return 'http://localhost:8080' //默认允许本地请求3000端口可跨域
    // },
    exposeHeaders: ['WWW-Authenticate', 'Server-Authorization'],
    maxAge: 5,
    credentials: true,
    allowMethods: ['GET', 'POST', 'DELETE','PUT'],
    allowHeaders: ['Content-Type', 'Authorization', 'Accept'],
}


module.exports = cors(corsOptions)