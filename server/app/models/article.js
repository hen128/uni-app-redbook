const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('article', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      comment: "文章主键ID"
    },
    title: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ""
    },
    description: {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: "文章简介"
    },
    content: {
      type: DataTypes.TEXT,
      allowNull: false,
      comment: "文章内容"
    },
    img: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: "",
      comment: "文章封面图"
    },
    seo: {
      type: DataTypes.STRING(20),
      allowNull: true,
      defaultValue: "",
      comment: "文章SEO关键字"
    },
    status: {
      type: DataTypes.TINYINT,
      allowNull: true,
      defaultValue: 1,
      comment: "文章展示状态：0-隐藏,1-正常 ，2 草稿，默认1"
    },
    sort: {
      type: DataTypes.TINYINT.UNSIGNED,
      allowNull: true,
      defaultValue: 1,
      comment: "排序编号"
    },
    userId: {
      type: DataTypes.INTEGER,
      allowNull: true,
      comment: "作者ID"
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'article',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
