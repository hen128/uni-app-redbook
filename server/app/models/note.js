const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('note', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      comment: "主键ID"
    },
    title: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: "",
      comment: "标题"
    },
    content: {
      type: DataTypes.TEXT,
      allowNull: false,
      comment: "内容"
    },
    carousel: {
      type: DataTypes.JSON,
      allowNull: true,
      comment: "轮播图"
    },
    status: {
      type: DataTypes.TINYINT,
      allowNull: true,
      defaultValue: 1,
      comment: "展示状态：0-隐藏,1-正常,默认1"
    },
    userId: {
      type: DataTypes.INTEGER,
      allowNull: true,
      comment: "作者ID"
    },
    like: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0,
      comment: "点赞数"
    },
    favorite: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0,
      comment: "收藏数"
    },
    browse: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0,
      comment: "浏览数"
    },
    topic: {
      type: DataTypes.JSON,
      allowNull: false,
      comment: "话题"
    },
    check: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 1,
      comment: "审核状态：0-待审核，1-通过，2-不通过，默认-0"
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'note',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
