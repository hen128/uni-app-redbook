var DataTypes = require("sequelize").DataTypes;
var _admin = require("./admin");
var _browse = require("./browse");
var _comment = require("./comment");
var _fans = require("./fans");
var _favorite = require("./favorite");
var _file = require("./file");
var _like = require("./like");
var _news = require("./news");
var _note = require("./note");
var _reply = require("./reply");
var _search = require("./search");
var _topic = require("./topic");
var _user = require("./user");

function initModels(sequelize) {
  var admin = _admin(sequelize, DataTypes);
  var browse = _browse(sequelize, DataTypes);
  var comment = _comment(sequelize, DataTypes);
  var fans = _fans(sequelize, DataTypes);
  var favorite = _favorite(sequelize, DataTypes);
  var file = _file(sequelize, DataTypes);
  var like = _like(sequelize, DataTypes);
  var news = _news(sequelize, DataTypes);
  var note = _note(sequelize, DataTypes);
  var reply = _reply(sequelize, DataTypes);
  var search = _search(sequelize, DataTypes);
  var topic = _topic(sequelize, DataTypes);
  var user = _user(sequelize, DataTypes);


  return {
    admin,
    browse,
    comment,
    fans,
    favorite,
    file,
    like,
    news,
    note,
    reply,
    search,
    topic,
    user,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
