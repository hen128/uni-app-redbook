const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      comment: "管理员主键ID"
    },
    phone: {
      type: DataTypes.STRING(50),
      allowNull: false,
      comment: "登录邮箱",
      unique: "admin_email_unique"
    },
    name: {
      type: DataTypes.STRING(50),
      allowNull: false,
      comment: "昵称"
    },
    brief: {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: "简介"
    },
    password: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: "3282facaf98ef20c816f3a91edcccc33",
      comment: "登录密码"
    },
    avatar: {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: "头像"
    },
    cover: {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: "封面"
    },
    status: {
      type: DataTypes.TINYINT,
      allowNull: false,
      defaultValue: 1,
      comment: "默认 1为  0为禁用"
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      comment: "创建时间"
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'user',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "admin_email_unique",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "phone" },
        ]
      },
    ]
  });
};
