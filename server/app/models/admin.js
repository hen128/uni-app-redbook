const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('admin', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      comment: "管理员主键ID"
    },
    account: {
      type: DataTypes.STRING(50),
      allowNull: true,
      comment: "账号"
    },
    name: {
      type: DataTypes.STRING(50),
      allowNull: false,
      comment: "昵称"
    },
    password: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: "3282facaf98ef20c816f3a91edcccc33",
      comment: "登录密码"
    },
    admin: {
      type: DataTypes.TINYINT,
      allowNull: false,
      defaultValue: 0,
      comment: "1为管理员 9超级管理员"
    },
    status: {
      type: DataTypes.TINYINT,
      allowNull: false,
      defaultValue: 1,
      comment: "默认 1为  0为禁用"
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      comment: "创建时间"
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'admin',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
