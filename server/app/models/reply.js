const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('reply', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    content: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    commentId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "回复人"
    },
    replyUserId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "被回复人ID"
    },
    status: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      comment: "0：审核中，1：审核通过，2：审核不通过，默认：1"
    },
    read: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0,
      comment: "阅读状态，0：未阅，1：已阅，默认：0"
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'reply',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
