const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('article_info', {
    articleId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      comment: "文章id"
    },
    like: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: true,
      defaultValue: 0,
      comment: "文章点赞次数"
    },
    browse: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: true,
      defaultValue: 0,
      comment: "文章浏览次数"
    }
  }, {
    sequelize,
    tableName: 'article_info',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "articleId" },
        ]
      },
    ]
  });
};
