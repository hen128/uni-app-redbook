const client = require('./client')
const file = require('./file')
const admin = require('./admin')

const InitRouter = (app) => {
    app.use(client.routes(), client.allowedMethods())
    app.use(admin.routes(), admin.allowedMethods())
    app.use(file.routes(), file.allowedMethods())
}
module.exports = InitRouter