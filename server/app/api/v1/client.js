const NoteController = require('../../controller/note')
const MsgController = require('../../controller/msg')
const UserController = require('../../controller/user')

// 路由前缀 /api/v1/client
const router = require('koa-router')({
  prefix: '/api/v1/client'
})
  // 获取话题排行前十
  .get('/topic/top', NoteController.getTopicTop)
  .get('/search/top', NoteController.getSearchTop)

  // 笔记
  .use('/note', require('koa-router')()
    // 笔记列表
    .get('/list', NoteController.listNote)
    // 笔记详情
    .get('/detail', NoteController.getDetail)
    // 删除笔记 作者本人
    .delete('/', NoteController.deleteNote)
    // 点赞笔记
    .put('/like', NoteController.addLike)
    // 加入收藏
    .put('/favorite', NoteController.addFavorite)
    // 笔记浏览记录+1
    .put('/browse', NoteController.addBrowse)
    // 发布笔记 & 保存草稿
    .post('/create', NoteController.createNote)
    // 搜素笔记
    .get('/search', NoteController.searchNote)
    .routes()
  )

  // 笔记
  .use('/msg', require('koa-router')()
    // 评论
    .post('/comment', MsgController.createComment)
    // 加载该笔记的评论及回复
    .get('/comment', MsgController.loadComments)
    // 回复
    .post('/reply', MsgController.createReply)
    // 删除评论或回复
    .delete('/', MsgController.deleteCommentOrReply)
    // 评论列表（后台管理）
    .get('/list', MsgController.listComment)
    // 审核
    .put('/check', MsgController.check)
    .routes()
  )

  // 个人
  .use('/user', require('koa-router')()
    .put('/news/read', UserController.readNews)
    .delete('/news', UserController.deleteNews)
    // 获取消息
    .get('/news', UserController.getNews)
    // 获取系统消息
    .get('/news/system', UserController.getNewsBySystem)
    // 获取关注关系
    .get('/relationShip', UserController.getRelationShip)
    // 关注
    .post('/focus', UserController.toFocus)
    // 加载作者点赞
    .get('/like', UserController.getLikeByUser)
    // 加载作者笔记
    .get('/favorite', UserController.getFavoriteByUser)
    // 加载作者笔记
    .get('/note', UserController.getNoteByUser)
    // 登录
    .post('/login', UserController.loginByPassword)
    // 注册
    .post('/register', UserController.registerUser)
    // 用户信息
    .get('/info', UserController.getUserInfo)
    // 我的关注
    .get('/focus', UserController.getUserFocus)
    // 我的粉丝
    .get('/fans', UserController.getUserFans)
    // 我的收藏
    .get('/favorites', UserController.loadFavorites)
    // 我的点赞
    .get('/likes', UserController.loadLikes)
    // 我的浏览
    .get('/browses', UserController.loadBrowses)
    // 修改手机号
    .put('/phone', UserController.editPhone)
    // 修改密码
    .put('/password', UserController.editPassword)
    // 编辑资料
    .put('/info', UserController.editInfo)
    // 搜素用户
    .get('/search', UserController.searchUser)
    .routes()
  )
module.exports = router