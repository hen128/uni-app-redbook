const Upload = require('../../utils/Upload')
const FileController = require('../../controller_file/index') // 文件管理


const router = require('koa-router')({
    prefix: '/api/v1/file'
})

// 更新文件
router.put('/', FileController.updateFile)
// 搜索文件
router.get('/search', FileController.searchFile)
// 文件详情
router.get('/file', FileController.getFile)
// 获取文件
router.get('/list', FileController.listFile)
// 获取文件树状目录
router.get('/tree', FileController.listTree)
// 生成文件
router.post('/create', FileController.createFile)
// 单文件上传
router.post('/upload', Upload.single('file'), FileController.uploadFile)
// 删除文件
router.delete('/', FileController.delFile)



module.exports = router