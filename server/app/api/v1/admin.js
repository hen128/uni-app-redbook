const AdminController = require('../../controller/admin')

// 路由前缀 /api/v1/client
const router = require('koa-router')({
    prefix: '/api/v1/admin'
})
    // 登录
    .post('/login', AdminController.loginAdminer)
    // 笔记列表
    .get('/note/list', AdminController.listNote)
    // 审核笔记
    .put('/note/check', AdminController.checkNote)
    // 用户列表
    .get('/user/list', AdminController.listUser)
    // 用户禁用or开放
    .put('/user/check', AdminController.checkUser)
    // 管理员列表
    .get('/admin/list', AdminController.listAdminer)
    
module.exports = router