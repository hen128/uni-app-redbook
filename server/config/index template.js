module.exports = {
    serve_config: {
        host: 'localhost',
        port: 3000
    },
    jwt_config: {
        serect: 'zengweihao.cn',// 盐
        expiresIn: '24h' // 60 * 60 或 '2 days'，'10h'，'7d' 
    },
    db_config: {
        dialect: 'mysql',
        host: "127.0.0.1",
        database: 'redbook',
        user: 'root',
        password: '123456',
        port: 3306
    }
}