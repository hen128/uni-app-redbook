const Sequlize = require('sequelize')
const initModels = require('../app/models/init-models')
const {
    dialect,
    host,
    database,
    user,
    password
} = global.config.db_config

const sequelize = new Sequlize(database, user, password, {
    host,
    dialect,
    dialectOptions: {
        charset: 'utf8mb4',
        dateStrings: true,
        typeCast: true
    },
    define: {
        timestamps: false,// 自动时间
    },
    timezone: '+08:00' //改为标准时区
})

module.exports = initModels(sequelize)