// 自动部署 根据 sql 生成 model
const SequlizeAuto = require('sequelize-auto')
const {dialect,host,database,user,password,port} = require('./config').db_config

const options = {
    dialect,
    host,
    directory: './app/models', // './ 指向文件根目录'
    port,
    additional:{
        timestamps:false
    }
}

const auto = new SequlizeAuto(database,user,password,options)

auto.run(err => {
    if(err) throw err
})