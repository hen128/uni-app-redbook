import { defineConfig } from 'vite'
import path from 'path'
import react from '@vitejs/plugin-react'
import WindiCSS from 'vite-plugin-windicss'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    react(),
    WindiCSS(),
  ],
  base:'./',
  resolve: {
    alias: {
      "@": path.resolve(__dirname, './src')
    }
  }
})
