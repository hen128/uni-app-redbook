// 这个文件专门定义请求参数的类型，和响应的类型
interface APIRes {
  code: number
  message: string
  data: {
    rows: string[],
    meta: any
  }
}
interface LgoinRes {
  code: number
  message: string
  data: {
    adminer: object,
    token: string
  }
}
interface PageSizeReq {
  page?: number
  size?: number
}
interface NoteCheckReq {
  noteId: number
  check: number,
  content?: string
}
interface UserCheckReq {
  userId: number
  status: number
}