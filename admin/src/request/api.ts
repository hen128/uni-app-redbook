import request from "./index"
// 管理员登录
export const httpLogin = (params: any): Promise<LgoinRes> => request.post('/admin/login', params)
// 获取笔记列表
export const httpNoteList = (params: PageSizeReq): Promise<APIRes> => request.get('/admin/note/list', {
    params
})
// 获取笔记列表
export const httpUserList = (params: PageSizeReq): Promise<APIRes> => request.get('/admin/user/list', {
    params
})
// 审核笔记
export const httpNoteCheck = (params: NoteCheckReq): Promise<APIRes> => request.put('/admin/note/check', params)
// 更改用户权限
export const httpUserCheck = (params: UserCheckReq): Promise<APIRes> => request.put('/admin/user/check', params)
// 查看用户详情
export const httpUserDetail = () => {
    request.get('/login')
}