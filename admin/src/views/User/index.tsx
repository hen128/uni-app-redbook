import React, { useState, useEffect } from 'react'
import { Space, Button, Avatar, Table, Tag, Pagination, message } from 'antd'
import { UserOutlined } from '@ant-design/icons'
import type { ColumnsType } from 'antd/es/table'
import { httpUserList, httpUserCheck } from "@/request/api"
import placeholderImg from '@/assets/placeholder.jpg'

interface DataType {
    id: number
    name: string
    phone: string
    avatar: string | null
    status: number
    createdAt: string
}

const View: React.FC = () => {
    const columns: ColumnsType<DataType> = [
        {
            title: 'ID',
            dataIndex: 'id',
            width: 100
        },
        {
            title: '作者',
            dataIndex: 'name',
            width: 200
        },
        {
            title: '头像',
            dataIndex: 'avatar',
            render: (img) => <Avatar src={img} icon={<UserOutlined />} />
        },
        {
            title: '手机号',
            dataIndex: 'phone',
            width: 200
        },
        {
            title: '账号状态',
            render: (_, record) => (
                <Tag>{record.status === 0 ? '待审核' : (record.status === 1 ? '正常' : '冻结')}</Tag>
            ),
        },
        {
            title: '操作',
            render: (_, record) => (
                <Space size="middle">
                    {record.status === 1 ? <Button onClick={() => { checkNote(record.id, 0) }} danger>冻结</Button> : <Button onClick={() => { checkNote(record.id, 1) }}>解封</Button>}
                </Space>
            ),
        },
    ]
    const [notes, setNotes] = useState<any[]>([])
    const [page, setPage] = useState(1)
    const [total, setTotal] = useState(0)
    // 切换页码
    const changPage = (page: number) => {
        setPage(page)
        fetchData(page)
    }
    // 请求数据
    const fetchData = async (page: number) => {
        const { code, data } = await httpUserList({ page, size: 10 })
        if (code === 200) {
            setNotes(data.rows)
            setTotal(data.meta.count)
        }
    }
    // 审核
    const checkNote = async (userId: number, status: number) => {
        const { code, message: msg, data } = await httpUserCheck({ userId, status })
        if (code === 200) {
            message.info('操作成功！')
            fetchData(page)
        }
    }
    useEffect(() => {
        fetchData(page)
    }, [])
    return (
        <>
            <Table columns={columns} pagination={false} rowKey={(record) => record.id} dataSource={notes} expandable={{
                expandedRowRender: (record) =>
                    <>
                        <div className='flex'><b>简介：</b><span>{record.brief}</span></div>
                        <div className='flex'><b>注册时间：</b><span>{record.createdAt}</span></div>
                    </>,
                rowExpandable: (record) => record.title !== 'Not Expandable'
            }} />
            <Pagination onChange={changPage} showSizeChanger={false} className='text-center my-6' pageSize={10} defaultCurrent={1} current={page} total={total} />
        </>
    )
}

export default View
