import React from 'react'
import { Button, Empty } from 'antd'

const App: React.FC = () => (
    <div className='flex h-screen justify-center items-center'>
        <Empty description={
            <p>404 NOT FOUNT</p>
        }>
            <Button type="primary"><a href="/">回到首页</a></Button>
        </Empty>
    </div>
)

export default App