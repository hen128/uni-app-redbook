import React from 'react'
import { useNavigate } from "react-router-dom"
import { Button, Checkbox, Form, Input, Card, message } from 'antd'
// 导入背景图，Vite导入图片方式
import bgPic from '@/assets/bg.jpg'
import { httpLogin } from "@/request/api"

const View: React.FC = () => {
    const navigateTo = useNavigate()
    // 正确提交
    const onFinish = async (values: any) => {
        const { code, message: msg, data } = await httpLogin(values)
        if (code === 200) {
            message.success('欢迎回来~')
            // 存储登录信息
            localStorage.setItem('token', JSON.stringify(data.token))
            localStorage.setItem('adminer', JSON.stringify(data.adminer))
            // 跳转到首页
            navigateTo('/')
        } else {
            message.error(msg)
        }
    }
    // 错误提交
    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo)
    }
    return (
        <div className='flex h-screen justify-center items-center bg-cover bg-center' style={{ backgroundImage: 'url(' + bgPic + ')' }}>
            <Card className='opacity-80' style={{ width: 420, height: 340 }}>
                <h1 className='text-center text-2xl pb-6'>管理员登录</h1>
                <Form
                    name="basic"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 16 }}
                    style={{ maxWidth: 600 }}
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                >
                    <Form.Item
                        label="账户"
                        name="account"
                        rules={[{ required: true, message: 'Please input your username!' }]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="密码"
                        name="password"
                        rules={[{ required: true, message: 'Please input your password!' }]}
                    >
                        <Input.Password />
                    </Form.Item>

                    <Form.Item name="remember" valuePropName="checked" wrapperCol={{ offset: 8, span: 16 }}>
                        <Checkbox>记住我</Checkbox>
                    </Form.Item>

                    <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                        <Button type="primary" htmlType="submit">
                            登录
                        </Button>
                    </Form.Item>
                </Form>
            </Card>
        </div>
    )
}

export default View