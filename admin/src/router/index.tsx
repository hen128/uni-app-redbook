import React, { lazy } from "react"
// Navigate重定向组件
import { Navigate } from "react-router-dom"

import Layout from "@/layout"
import Login from "@/views/Login"
// const Login = lazy(() => import("@/views/Login"))
const Home = lazy(() => import("@/views/Home"))
const Note = lazy(() => import("@/views/Note"))
const User = lazy(() => import("@/views/User"))
const NotFount = lazy(() => import("@/views/404"))

// 报错A component suspended while responding to synchronous input. This will cause the UI to be replaced with a loading indicator.
// 懒加载的模式的组件的写法，外面需要套一层 Loading 的提示加载组件
const withLoadingComponent = (children: JSX.Element) => (
    <React.Suspense fallback={<div className="text-center">Loading...</div>}>
        {children}
    </React.Suspense>
)

const routes = [
    {
        path: "/",
        element: <Navigate to="/home" /> // 重定向
    },
    {
        path: "/",
        element: <Layout />,
        children: [
            {
                path: "/home",
                element: withLoadingComponent(<Home />),
                meta: {
                    title: '首页',
                },
            },
            {
                path: "/user",
                element: withLoadingComponent(<User />),
                meta: {
                    title: '用户',
                },
            },
            {
                path: "/note",
                element: withLoadingComponent(<Note />),
                meta: {
                    title: '笔记',
                },
            }
        ]
    },
    {
        path: "/login",
        // element: withLoadingComponent(<Login />)
        element: <Login />
    },
    {
        path: "*",
        element: withLoadingComponent(<NotFount />)
    }
]

export default routes