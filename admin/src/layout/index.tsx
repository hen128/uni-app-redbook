import React from 'react'
import { Outlet } from "react-router-dom"
import { Layout, } from 'antd'
const { Content } = Layout
import MyHeader from '@/layout/Header'
import MyFooter from '@/layout/Footer'
import MySider from '@/layout/Sider'
const App: React.FC = () => {
    return (
        <Layout style={{ minHeight: '100vh' }}>
            <MySider />
            <Layout>
                <MyHeader />
                <Content className='m-4'>
                    <Outlet />
                </Content>
                <MyFooter />
            </Layout>
        </Layout>
    )
}
export default App