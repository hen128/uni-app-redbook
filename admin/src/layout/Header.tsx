import React from 'react'
import { useNavigate } from "react-router-dom"
import { DownOutlined, ExclamationCircleFilled } from '@ant-design/icons'
import { Layout, Dropdown, Space, Modal } from 'antd'
const { Header } = Layout
const { confirm } = Modal
import type { MenuProps } from 'antd'
const items: MenuProps['items'] = [
    {
        key: '1',
        label: ('关于'),
    },
    {
        key: '2',
        label: ('注销'),
    },
]
const View: React.FC = () => {
    const navigateTo = useNavigate()
    // 获取登录信息
    const getAdminerInfo = () => {
        const adminer = localStorage.getItem('adminer') || ''
        try {
            const user = JSON.parse(adminer)
            if (user) {
                return user.name
            } else {
                return "请先登录"
            }
        } catch (error) {
            console.error(error)
        }
    }
    // 注销
    const logout = () => {
        confirm({
            title: '是否注销该账号',
            icon: <ExclamationCircleFilled />,
            content: '',
            onOk() {
                localStorage.removeItem("token")
                // 跳转到首页
                navigateTo('/login')
            },
            onCancel() {
                console.log('Cancel')
            },
        })
    }
    const onClick: MenuProps['onClick'] = ({ key }) => {
        switch (key) {
            case '1':
                console.log('关于')
                break
            case '2':
                logout()
                break
        }
    }
    return (
        <Header>
            <Dropdown className='float-right text-white' menu={{ items, onClick }}>
                <Space>
                    {getAdminerInfo()}
                    <DownOutlined />
                </Space>
            </Dropdown>
        </Header>
    )
}

export default View