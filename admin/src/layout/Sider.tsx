import React, { useState } from 'react'
import {
    FileSearchOutlined,
    PieChartOutlined,
    UserOutlined,
} from '@ant-design/icons'
import { useNavigate, useLocation } from "react-router-dom"
import type { MenuProps } from 'antd'
import { Layout, Menu } from 'antd'
const { Sider } = Layout

type MenuItem = Required<MenuProps>['items'][number]

function getItem(
    label: React.ReactNode,
    key: React.Key,
    icon?: React.ReactNode,
    children?: MenuItem[],
): MenuItem {
    return {
        key,
        icon,
        children,
        label,
    } as MenuItem
}

const items: MenuItem[] = [
    getItem('首页', '/home', <PieChartOutlined />),
    getItem('笔记审核', '/note', <FileSearchOutlined />),
    getItem('用户管理', '/user', <UserOutlined />),
]

const App: React.FC = () => {
    // 高亮当前菜单
    let activeIndex = '/home'
    const location = useLocation()
    activeIndex = location.pathname

    const navigateTo = useNavigate()
    const [collapsed, setCollapsed] = useState(false)

    const goto: MenuProps['onClick'] = (e) => {
        navigateTo(e.key)
    }

    return (
        <Sider collapsible collapsed={collapsed} onCollapse={(value) => setCollapsed(value)}>
            <div className="h-8 m-4 leading-8 text-center align-middle bg-light-900">后台管理</div>
            <Menu theme="dark" onClick={goto} defaultSelectedKeys={[activeIndex]} mode="inline" items={items} />
        </Sider>
    )
}

export default App