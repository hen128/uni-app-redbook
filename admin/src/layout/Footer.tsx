import React from 'react'
import {Layout } from 'antd';
const {  Footer } = Layout

const View: React.FC = () => {
    return (
        <Footer className='text-center'>Ant Design ©2023 Created by Ant UED</Footer>
    )
};

export default View